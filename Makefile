#!make

all:
	@echo "Project aliases and shortcuts."
	@echo "  make cc                            - Clear cache"
	@echo "  make deploy                        - Deploy app"
	@echo "  make phs                           - Phinx migrations status"
	@echo "  make phm                           - Phinx migrations migrate"
	@echo "  make docker:cc                     - Stop containers, remove containers and images"
	@echo "  make docker:deploy                 - Deploys docker app"

help:
	make all

cc:
	./bin/cleantemp.sh