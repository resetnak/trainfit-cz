-- Adminer 4.7.5 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `admin`;
CREATE TABLE `admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `surname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nick` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `role` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `full_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `hour_price` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `admin` (`id`, `name`, `surname`, `nick`, `password`, `email`, `role`, `created_at`, `full_name`, `hour_price`) VALUES
(1,	'Alex',	'Rešetňak',	'alexroot',	'$2y$10$5vG87r9iWNHrUnv17uNw6.T2Wa7RzP/lCukWUI8KKluXiGsz3am46',	'alex@resetnak.cz',	'superadmin',	'2019-09-20 22:51:06',	'Alex Rešetňak',	700),
(2,	'Testovaci',	'Trener',	'test_trener',	'$2y$10$5vG87r9iWNHrUnv17uNw6.T2Wa7RzP/lCukWUI8KKluXiGsz3am46',	'trener@resetnak.cz',	'trainer',	'2020-01-23 17:11:36',	'Testovaci Trener',	700),
(3,	'František',	'Och',	'och',	'$2y$10$5vG87r9iWNHrUnv17uNw6.T2Wa7RzP/lCukWUI8KKluXiGsz3am46',	'',	'superadmin',	'2020-02-07 13:51:49',	'František Och',	800),
(4,	'Barbora',	'Kapounová',	'kapounova',	'$2y$10$5vG87r9iWNHrUnv17uNw6.T2Wa7RzP/lCukWUI8KKluXiGsz3am46',	'',	'trainer',	'2020-02-07 13:52:34',	'Barbora Kapounová',	700),
(5,	'Sebastian',	'Balín',	'balin',	'$2y$10$5vG87r9iWNHrUnv17uNw6.T2Wa7RzP/lCukWUI8KKluXiGsz3am46',	'',	'trainer',	'2020-02-07 13:52:34',	'Sebastian Balín',	700),
(6,	'Robert',	'Hlušička',	'hlusicka',	'$2y$10$5vG87r9iWNHrUnv17uNw6.T2Wa7RzP/lCukWUI8KKluXiGsz3am46',	'',	'trainer',	'2020-02-07 13:52:34',	'Robert Hlušička',	700),
(7,	'Kristýna',	'Jelínková',	'jelinkova',	'$2y$10$5vG87r9iWNHrUnv17uNw6.T2Wa7RzP/lCukWUI8KKluXiGsz3am46',	'',	'trainer',	'2020-02-07 13:54:00',	'Kristýna Jelínková',	700);

DROP TABLE IF EXISTS `event`;
CREATE TABLE `event` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` int(11) DEFAULT NULL,
  `trainer` int(11) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `start` datetime NOT NULL,
  `end` datetime NOT NULL,
  `break` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_3BAE0AA7C5150820` (`trainer`),
  KEY `IDX_3BAE0AA78D93D649` (`user`),
  CONSTRAINT `FK_3BAE0AA78D93D649` FOREIGN KEY (`user`) REFERENCES `user` (`id`),
  CONSTRAINT `FK_3BAE0AA7C5150820` FOREIGN KEY (`trainer`) REFERENCES `admin` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `event` (`id`, `user`, `trainer`, `title`, `start`, `end`, `break`) VALUES
(163,	3,	2,	'dada',	'2020-02-03 06:00:00',	'2020-02-03 06:15:00',	'no'),
(165,	3,	2,	'dddd',	'2020-01-27 08:30:00',	'2020-01-27 08:45:00',	'no'),
(166,	3,	2,	'dadada',	'2020-02-05 06:00:00',	'2020-02-05 06:15:00',	'yes'),
(193,	NULL,	NULL,	'Přestávka',	'2020-02-06 07:00:00',	'2020-02-06 07:30:00',	'no'),
(194,	NULL,	NULL,	'Přestávka',	'2020-02-06 07:00:00',	'2020-02-06 07:30:00',	'yes'),
(195,	NULL,	NULL,	'Přestávka',	'2020-02-06 07:00:00',	'2020-02-06 07:30:00',	'no'),
(196,	NULL,	1,	'Přestávka',	'2020-02-06 07:00:00',	'2020-02-06 07:30:00',	'yes'),
(197,	NULL,	1,	'Přestávka',	'2020-02-07 07:00:00',	'2020-02-07 07:30:00',	'no'),
(198,	NULL,	1,	'Přestávka',	'2020-02-08 07:00:00',	'2020-02-08 07:30:00',	'yes'),
(199,	NULL,	1,	'Přestávka',	'2020-02-04 07:00:00',	'2020-02-04 07:30:00',	'yes'),
(200,	NULL,	1,	'Přestávka',	'2020-02-05 07:00:00',	'2020-02-05 07:30:00',	'no'),
(201,	NULL,	1,	'Přestávka',	'2020-02-03 07:00:00',	'2020-02-03 07:30:00',	'yes'),
(202,	NULL,	1,	'Nohy',	'2020-02-04 08:00:00',	'2020-02-04 09:00:00',	'no'),
(203,	3,	1,	'Trénink prsa',	'2020-02-05 08:00:00',	'2020-02-05 08:30:00',	NULL),
(204,	3,	1,	'test',	'2020-02-06 08:00:00',	'2020-02-06 08:30:00',	NULL),
(205,	NULL,	1,	'Přestávka',	'2020-02-07 08:00:00',	'2020-02-07 08:30:00',	'yes'),
(206,	3,	2,	'test',	'2020-02-04 06:30:00',	'2020-02-04 07:00:00',	NULL),
(207,	3,	1,	'test',	'2020-02-05 10:00:00',	'2020-02-05 11:00:00',	NULL),
(208,	3,	1,	'Alexandr Rešetňak',	'2020-02-06 10:00:00',	'2020-02-06 11:00:00',	NULL),
(209,	NULL,	1,	'Přestávka',	'2020-02-07 10:00:00',	'2020-02-07 11:00:00',	'yes'),
(210,	NULL,	1,	'Felali',	'2020-02-08 10:00:00',	'2020-02-08 11:00:00',	'no'),
(211,	NULL,	5,	'Pani X',	'2020-02-10 10:00:00',	'2020-02-10 11:00:00',	'no'),
(212,	3,	2,	'Alexandr Rešetňak',	'2020-02-11 17:00:00',	'2020-02-11 18:00:00',	NULL),
(213,	NULL,	2,	'Přestávka',	'2020-02-12 08:00:00',	'2020-02-12 09:00:00',	'yes'),
(214,	NULL,	3,	'Felali',	'2020-02-12 08:00:00',	'2020-02-12 09:00:00',	'no'),
(215,	NULL,	3,	'test',	'2020-02-11 08:00:00',	'2020-02-11 09:00:00',	'no'),
(217,	NULL,	3,	'Přestávka',	'2020-02-13 08:00:00',	'2020-02-13 09:00:00',	'yes'),
(218,	NULL,	3,	'Přestávka',	'2020-02-12 10:00:00',	'2020-02-12 11:00:00',	'yes'),
(220,	NULL,	3,	's',	'2020-02-14 08:00:00',	'2020-02-14 09:00:00',	'no'),
(221,	NULL,	3,	'x',	'2020-02-13 13:00:00',	'2020-02-13 14:00:00',	'no');

DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nickname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `role` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `generated_ad` datetime DEFAULT NULL,
  `full_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `package_amount` int(11) DEFAULT NULL,
  `allowed_trainers` longtext COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '(DC2Type:array)',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `user` (`id`, `uid`, `first_name`, `last_name`, `nickname`, `password`, `email`, `role`, `created_at`, `generated_ad`, `full_name`, `package_amount`, `allowed_trainers`) VALUES
(3,	'a7nvq6ov5t3a',	'Alexandr',	'Rešetňak',	'alex',	'$2y$10$rh5CGVFu88Dj/y8xTTn2XOB.7GSgPrQk1ar9B0.i2FWZS7QmsAlPW',	'mail@resetnak.cz',	'user',	'2019-10-08 00:20:02',	'2019-10-06 18:08:58',	'Alexandr Rešetňak',	11,	NULL),
(6,	'cjhqxg3sajlu',	'Pepa',	'Pepovič',	'pepa',	'$2y$10$3Vv1uJAWcPDR7SZ1903Wpu3200FXKzPcXuiv8ujtLA98FKv52MMyq',	'mail@resetnak.cz',	'user',	'2019-10-08 00:21:32',	'2019-10-06 19:18:43',	'Pepa Pepovič',	0,	NULL),
(7,	'n7clyu29m99t',	'Alexandr',	'Rešetňak',	'alexandr',	'$2y$10$FSIc64TcXLdtwTCQp4nmm.XLu5MeVO0E.9Jxw.hXQmZclst31Jy1W',	'mail@resetnak.cz',	'user',	'2019-10-08 00:28:20',	'2019-10-08 00:27:47',	'Alexandr Rešetňak',	5,	NULL),
(9,	'4jh3pjwd1lle',	'Alex',	'Reset',	'Test',	'$2y$10$w2fFan1Vyx9YTMS33m9VBuKPOYuec/x2ZQFh5ZPK8YtnUB2Jfv/4K',	'mail@resetnak.cz',	'user',	'2019-10-08 01:08:04',	'2019-10-08 01:06:42',	'Alex Reset',	21,	NULL),
(10,	'siksj2j84lmf',	'Alex',	'Test',	'testroot',	'$2y$10$GRbZ2TGNlKRYPDgQCUcIIux3NGYEj.W2cbVK9PQVjOM8LeaZiEf9a',	'sabro.1661@gmail.com',	'user',	'2019-10-08 01:55:36',	'2019-10-08 01:52:59',	'Alex Test',	0,	NULL),
(11,	'tbmzjyaak6kz',	'Franta',	'jajj',	'fany',	'$2y$10$jGfkM6lgbdySU7jPVKRuvu/72NDrxW4DBeqlQnFUwVllBqi6N0qkm',	'fany98@seznam.cz',	'user',	'2019-10-08 10:51:12',	'2019-10-08 09:42:55',	'Franta jajj',	0,	NULL),
(12,	'5ecucfdtg802',	'Alex',	'Resetnak',	'alextestik',	'$2y$10$NF4vxrbXmVp7z6pw5QNND.aef6GCoYD3IKhVqLbduyWfSTVn5bdhm',	'x@ss.cc',	'user',	'2020-02-16 16:12:59',	'2020-02-16 16:12:14',	' ',	NULL,	'a:4:{i:0;i:2;i:1;i:3;i:2;i:5;i:3;i:7;}'),
(13,	'lwq6676cxo5o',	'Te',	'St',	'testgenerovani',	'$2y$10$wtB738uPvlw1pCqTm6hSTe.N.iCa6wN0yyMmW2.CtyjCnVWld6Fga',	'a.resetneac@gmail.com',	'user',	'2020-02-16 19:17:39',	'2020-02-16 19:16:56',	' ',	NULL,	'a:2:{i:0;i:6;i:1;i:7;}'),
(14,	NULL,	NULL,	NULL,	'68gwleb2',	'f1r8zh2q',	'xxxx@xxxx.xx',	'user',	NULL,	'2020-02-16 20:07:38',	' ',	0,	'a:1:{i:0;i:5;}');

-- 2020-02-16 19:24:09