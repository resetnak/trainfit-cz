<?php declare(strict_types=1);

namespace App\AdminModule\Presenters;

use App\Services\Admin\Forms\AdminLoginFormFactory;
use App\Services\Admin\Forms\GenerateUserFormFactory;
use App\Services\Database\Entities\Admin;
use App\Services\Database\Entities\User;
use App\Services\Database\Repositories\Users;
use Nette\Application\UI\Form;
use Nette\Mail\Message;
use Nette\Mail\SendmailMailer;
use Nette\Mail\SmtpMailer;
use Nette\Security\AuthenticationException;
use Nette\Utils\Random;
use Tracy\Debugger;

final class AdminPresenter extends BasePresenter
{

    /** @var AdminLoginFormFactory @inject */
    public $adminLoginFormFactory;

    /** @var GenerateUserFormFactory @inject */
    public $generateUserFormFactory;

    /** @var Admin @inject */
    public $admin;

    /** @var Users @inject */
    public $_users;

    public function startup()
    {
        // Parent call
        parent::startup();

        if($this->user->isLoggedIn() && $this->presenter->isLinkCurrent(':default')) {
            $this->redirect(':dashboard');
        }
    }

    public function createComponentGenerateUser(): Form
    {
        $trainers = $this->admins->getTrainersForSelect();

        $form = $this->generateUserFormFactory->create($trainers);

        $form->onSuccess[] = [$this, 'processGenerateUser'];

        return $form;
    }

    public function processGenerateUser(Form $form)
    {
        $data = $form->getValues();

        $getUser = $this->users->getByEmail($data->email);

        if ($getUser instanceof User) {
            $this->flashMessage('Uživatel s tímto emailem již existuje.', 'warning');
            $this->redirect('this');
        } else {
            $username = Random::generate(8);
            $password = Random::generate(8);

            $user = new User();
            $user->setFirstName($data->name);
            $user->setLastName($data->surname);
            $user->setFullName($data->name . ' ' . $data->surname);
            $user->setNickname($username);
            $user->setPassword($password);
            $user->setEmail($data->email);
            $user->setAllowedTrainers($data->trainers);

            $this->_users->create($user);

            $mail = new Message();
            $mail->setFrom('TrainFit <mail@trainfit.cz>')
                ->addTo($data->email)
                ->setSubject('Přístupové údaje')
                ->setBody("Dobrý den,\nzde jsou Vaše přístupové údaje do systému trainfit.cz/user.\nPřihlašovací jméno: " . $username . " \nHeslo: " . $password . "");

            $this->mailer->send($mail);

            $this->flashMessage('Uživatel vygenerován', 'success');
            $this->redirect('this');
        }


    }

    public function createComponentAdminLoginForm(): Form
    {
        $form = $this->adminLoginFormFactory->create();
        $form->onSuccess[] = [$this, 'processAdminLoginForm'];
        return $form;
    }

    public function processAdminLoginForm(Form $form)
    {
        $values = $form->getValues(true);
        $user = $this->getUser();
        try {
            $user->login($values['login'], $values['password']);
            $this->user->setExpiration('100 minutes');
            if ($user->isLoggedIn()) {
                $this->redirect(':Admin:Calendar:');
            }
        } catch (AuthenticationException $e) {
            $this->flashMessage('Špatně zadané jméno nebo heslo.', 'warning');
            Debugger::log($e);
            $this->redirect('this');
        }
    }

}
