<?php declare(strict_types = 1);

namespace App\AdminModule\Presenters;

use App\Services\Database\Entities\Admin;
use App\Services\Database\Repositories\Admins;
use Nette\Security\AuthenticationException;
use Nette\Security\IAuthenticator;
use Nette\Security\Identity;
use Nette\Security\Passwords;

class AdminAuthenticator implements IAuthenticator
{

    /** @var Admins */
    private $admins;

    public function __construct(Admins $admins)
    {
        $this->admins = $admins;
    }

    public function authenticate(array $credentials)
    {
        list($nick, $password) = $credentials;

        $admin = $this->admins->getByNick($nick);

        if (!$admin instanceof Admin)
        {
            throw new AuthenticationException('Špatně zadané údaje.');
        }

        $verify = Passwords::verify($password, $admin->getPassword());

        if (!$verify)
        {
            throw new AuthenticationException('Špatně zadané údaje.');
        }

        return new Identity($admin->getId(), $admin->getRole());
    }

}