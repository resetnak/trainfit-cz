<?php declare(strict_types = 1);

namespace App\AdminModule\Presenters;

use App\Services\Admin\Forms\AdminCalendarModalFormFactory;
use App\Services\Database\Entities\Admin;
use App\Services\Database\Entities\Event;
use App\Services\User\Components\CalendarEventFactory;
use App\Services\User\Forms\SelectTrainerFormFactory;
use Nette\Application\UI\Form;
use Nette\Utils\DateTime;
use Nette\Utils\Random;
use Tracy\Debugger;

class CalendarPresenter extends BasePresenter
{
    /** @var AdminCalendarModalFormFactory @inject */
    public $adminCalendarModalForm;

    /** @var CalendarEventFactory @inject */
    public $calendarEventFactory;

    /** @var SelectTrainerFormFactory @inject */
    public $selectTrainerFormFactory;

    public function handleSaveAdminEvent($start, $end, $title, $trainerBreak, $repeat_on)
    {
        if($this->isAjax())
        {
            /** @var Admin $trainer */
            $trainer = $this->admins->getById($this->getUser()->getIdentity()->getId());

            $newEvent = $this->calendarEventFactory->createAdminEvent($start, $end, $title, $trainer, $trainerBreak, $repeat_on);
            $this->template->calendarEvents = $this->calendarEventFactory->mapEvents($newEvent);

            $this->redrawControl('calendar');
            $this->redirect('this');
        }

    }

    public function handleUnselectEvent()
    {
        $this->redirect('this');
    }

    public function createComponentSelectTrainerForm(): Form
    {
        $trainers = $this->admins->getTrainersForSelect();

        $form = $this->selectTrainerFormFactory->create($trainers);
        $form->onSuccess[] = [$this, 'processSelectTrainer'];

        return $form;
    }

    public function processSelectTrainer(Form $form)
    {
        $data = $form->getValues();

        $trainerId = $data->trainer_id;

        if($trainerId !== null) {
            $this->redirect(':calendar', $trainerId);
        }
    }

    public function renderCalendar($trainerId)
    {
        $trainer = $this->admins->getById((int)$trainerId);
        $this->template->trainerName = $trainer->getFullName();
        $this->template->calendarEvents = $this->calendarEventFactory->getTrainerEvents($trainer);
    }

    public function renderDefault()
    {
        /** @var Admin $trainer */

        $trainer = $this->admins->getById($this->getUser()->getIdentity()->getId());
        $this->template->calendarEvents = $this->calendarEventFactory->getTrainerEvents($trainer);
        $this->template->schedule = [$trainer->getWorkingFrom(), $trainer->getWorkingTo()];
    }

    public function createComponentAdminCalendarModalForm(): Form
    {
        $form = $this->adminCalendarModalForm->create();

        $form->onSuccess[] = [$this, 'processAdminCalendarModal'];

        return $form;
    }

    public function processAdminCalendarModal(Form $form)
    {
        $data = $form->getValues();
    }

    public function handleDeleteEvent($eventId)
    {
        /** @var Event $event */
        $event = $this->events->getById((int)$eventId);

        if ($event->getUser() === null) {
            $this->events->delete($event);

            $this->redirect('this');
        } else {
            $user = $event->getUser();
            $user->addOnePackage();

            $this->users->update($user);

            $this->events->delete($event);

            $this->redirect('this');
        }
    }
}