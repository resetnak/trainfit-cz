<?php declare(strict_types=1);

namespace App\AdminModule\Presenters;

use App\Services\Admin\Forms\CalendarSettingsFormFactory;
use App\Services\Admin\Forms\SelectUserForPackageFormFactory;
use App\Services\Database\Entities\Admin;
use App\Services\Database\Entities\User;
use App\Services\User\Forms\TrainerSettingsFormFactory;
use Nette\Application\UI\Form;
use Nette\Forms\Container;
use Nette\Security\Passwords;
use Nette\Utils\ArrayHash;
use Nette\Utils\Html;
use Tracy\Debugger;
use Ublaboo\DataGrid\DataGrid;

class SettingsPresenter extends BasePresenter
{
    /** @var TrainerSettingsFormFactory @inject */
    public $trainerSettingsFormFactory;

    /** @var SelectUserForPackageFormFactory @inject */
    public $selectUserForPackageFormFactory;

    /** @var CalendarSettingsFormFactory @inject */
    public $calendarSettingsFormFactory;

    public function createComponentSelectUserForm(): Form
    {
        $users = $this->users->getUsersForSelect();
        return $this->selectUserForPackageFormFactory->create($users);
    }

    public function handlePackageSelection($data)
    {
        if ($this->isAjax()) {
            $ajaxValues = json_decode($data);
            /** @var User $user */
            $user = $this->users->getById((int)$ajaxValues->userId);
            $user->addPackageAmount((int)$ajaxValues->packageAmount);
            $this->users->update($user);
            $this->flashMessage('Úspěšně přidáno', 'success');
            $this->redirect('this');
        }
    }

    public function createComponentUserOverview(): DataGrid
    {
        $grid = new DataGrid();
        $grid->setDataSource($this->users->getAll());
        $grid->addColumnText('first_name', 'Jméno');
        $grid->addColumnText('last_name', 'Příjmení');
        $grid->addColumnText('email', 'Email');
        $grid->addColumnDateTime('created_at', 'Vytvořen');
        $grid->addColumnNumber('package_amount', 'Počet balíčků');

        return $grid;
    }

    public function createComponentCalendarSettingsForm()
    {
        return $this->calendarSettingsFormFactory->create();
    }

    public function handleSaveCalendarSettings($data)
    {
        if ($this->isAjax()) {
            $data = json_decode($data);

            /** @var Admin $trainer */
            $trainer = $this->admins->getById($this->getUser()->getIdentity()->getId());

            $trainer->setWorkingFrom($data->from);
            $trainer->setWorkingTo($data->to);

            $this->admins->update($trainer);
        }
    }

    public function createComponentTrainersPriceList()
    {
        $grid = new DataGrid();
        $grid->setDataSource($this->admins->getForGrid());

        $grid->getInlineEdit()->onCustomRedraw[] = function() use ($grid) {
            $grid->redrawControl();
        };

        $grid->addColumnText("id","ID")
            ->setAlign('center');
        $grid->addColumnText("name","Jméno")
            ->setAlign('center');
        $grid->addColumnText("surname","Příjmení")
            ->setAlign('center');
        $grid->addColumnText('email', 'Email')
            ->setAlign('center');
        $grid->addColumnText('role', 'Role')
            ->setAlign('center');
        $grid->addColumnText('hour_price', 'Hodinovka')
            ->setAlign('center')
            ->setEditableCallback([$this, 'priceEdited']);

        return $grid;

    }

    public function priceEdited($id, $value)
    {
        /** @var Admin $trainer */
         $trainer = $this->admins->getById((int)$id);

         $trainer->setHourPrice((int)$value);
         $this->admins->update($trainer);
    }

    public function renderInfo(): void
    {
        /** @var Admin $trainer */
        $trainer = $this->admins->getById($this->getUser()->getIdentity()->getId());

        $numOfEvents = count($this->events->getBookEventsUntilNowByTrainer($trainer));

        $made = ($numOfEvents * $trainer->getHourPrice()) - (($numOfEvents * $trainer->getHourPrice() * 0.15));

        $this->template->email = $trainer->getEmail();
        $this->template->nickname = $trainer->getNick();
        $this->template->trainer = $trainer->getFullName();
        $this->template->made = $made;
    }

    public function createComponentSettingsForm(): Form
    {
        $form = $this->trainerSettingsFormFactory->create();
        $form->onSuccess[] = [$this, 'processSettings'];
        return $form;
    }

    public function processSettings(Form $form)
    {
        $data = $form->getValues();
        /** @var Admin $trainer */
        $trainer = $this->admins->getById($this->getUser()->getIdentity()->getId());
        if ((isset($data->email)) && (!empty($data->email))) {
            $trainer->setEmail($data->email);
        }
        if ((isset($data->password)) && (!empty($data->password))) {
            $trainer->setPassword(Passwords::hash($data->password));
        }
        if ((isset($data->nickname)) && (!empty($data->nickname))) {
            $trainer->setNick($data->nickname);
        }
        $this->admins->update($trainer);
        $this->flashMessage('Uloženo', 'success');
        $this->redirect('this');

    }
}