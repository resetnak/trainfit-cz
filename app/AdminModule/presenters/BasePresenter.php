<?php

namespace App\AdminModule\Presenters;

use App\Services\Database\Entities\Event;
use App\Services\Database\Repositories\Admins;
use App\Services\Database\Repositories\Events;
use App\Services\Database\Repositories\Users;
use App\Services\Email\MailerFactory;
use Kdyby\Translation\Translator;
use Nette\Application\UI\Presenter;
use Tracy\Debugger;

abstract class BasePresenter extends Presenter
{
	/** @var @persistent */
	public $locale;

	/** @var Translator @inject */
	public $translator;

	/** @var MailerFactory @inject */
	public $mailer;

    /** @var Admins @inject */
    public $admins;

    /** @var Users @inject */
    public $users;

    /** @var Events @inject */
    public $events;

    /** @var Event @inject */
    public $event;

    public function startup()
    {
        // Parent call
        parent::startup();

        $this->setLayout(__DIR__ . '/../../template/@admin.latte');

        if (!$this->user->isLoggedIn())
        {
            if (!$this->presenter->isLinkCurrent(':Admin:Admin:')) {
                $this->flashMessage('Přihlaste se prosím', 'warning');
                $this->redirect(':Admin:Admin:');
            }
        }
	}

    public function handleLogout()
    {
        $this->user->logout(true);
        $this->redirect(':Public:Homepage:');
    }

}