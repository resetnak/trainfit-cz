<?php declare(strict_types = 1);

namespace App\AdminModule\Presenters;

use App\Services\Database\Entities\Admin;
use Ublaboo\DataGrid\DataGrid;

class CalculationPresenter extends BasePresenter
{
    public function createComponentCalculationGrid()
    {
        $grid = new DataGrid();
        $trainers = $this->admins->getForGrid();

        $grid->setDataSource($trainers);

        $grid->addColumnText('id', 'Id')
            ->setAlign('center');

        $grid->addColumnText('full_name', 'Jméno')
            ->setAlign('center');

        $grid->addColumnText('hour_price', 'Hodinovka')
            ->setAlign('center');

        $grid->addColumnText('hours', 'Odpracováno hodin')
            ->setRenderer(function (Admin $item){
               return count($this->events->getBookEventsUntilNowByTrainer($item));
            })
            ->setAlign('center');

        $grid->addColumnNumber('made', 'Vyděláno aktuálně')
            ->setRenderer(function (Admin $item){

                $numOfEvents = count($this->events->getBookEventsUntilNowByTrainer($item));

                return $numOfEvents * $item->getHourPrice();

            })->setAlign('center');

        $grid->addColumnNumber('madeTrainer', 'Vydělal trenér')
            ->setRenderer(function (Admin $item){

                $numOfEvents = count($this->events->getBookEventsUntilNowByTrainer($item));

                return ($numOfEvents * $item->getHourPrice()) - (($numOfEvents * $item->getHourPrice() * 0.15));

            })->setAlign('center');

        $grid->addColumnNumber('madeClean', 'Vydělal TrainFit')
            ->setRenderer(function (Admin $item){

                $numOfEvents = count($this->events->getBookEventsUntilNowByTrainer($item));

                return ($numOfEvents * $item->getHourPrice()) * 0.15;

            })->setAlign('center');

        return $grid;
    }
}