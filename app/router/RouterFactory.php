<?php

namespace App;

use Nette;
use Nette\Application\Routers\Route;
use Nette\Application\Routers\RouteList;


final class RouterFactory
{
    use Nette\StaticClass;

    /**
     * @return RouteList
     */
    public static function createRouter() : Nette\Application\IRouter
    {
        $router = new RouteList;

        $admin = new RouteList('Admin');
        $admin[] = new Route('admin/<presenter>/<action>[/<id>]', 'Admin:default');
        $router[] = $admin;

        $user = new RouteList('User');
        $user[] = new Route('user/<presenter>/<action>[/<id>]', 'User:default');
        $router[] = $user;

        $public = new RouteList('Public');
        $public[] = new Route('<presenter>/<action>[/<id>]', 'Homepage:default');
        $router[] = $public;

        return $router;
    }
}