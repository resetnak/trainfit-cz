<?php

namespace App\PublicModule\Presenters;

use App\Services\Database\Repositories\Admins;
use Kdyby\Translation\Translator;
use Nette\Application\UI\Presenter;

abstract class BasePresenter extends Presenter
{
	/** @var @persistent */
	public $locale;

	/** @var Translator @inject */
	public $translator;

	/** @var Admins @inject */
	public $admins;

    public function startup()
    {
        // Parent call
        parent::startup();

        $this->setLayout(__DIR__ . '/../../template/@public.latte');
	}

}