<?php

namespace App\PublicModule\Presenters;


use App\Services\Factories\ContactForm;
use Nette\Application\UI\Form;
use Nette\Mail\Message;
use Nette\Mail\SendmailMailer;

class HomepagePresenter extends BasePresenter
{
    /** @var ContactForm @inject */
    public $contactFormFactory;

    public function createComponentContactForm()
    {
        $form = $this->contactFormFactory->create();

        $form->onSubmit[] = [$this, 'processContactForm'];

        return $form;
    }

    public function renderPricelist()
    {
        $trainers = $this->admins->getForFrontPriceList();

        $this->template->priceLists = $trainers;
    }

    public function renderDefault()
    {
        $trainers = $this->admins->getForFrontPriceList();

        $detect = new \Mobile_Detect();

        $this->template->mobile = $detect->isMobile();
        $this->template->priceLists = $trainers;
    }

    public function processContactForm(Form $form)
    {
        $data = $form->getValues();

        $mail = new Message;
        $mail->setFrom($data->email)
            ->addTo('info@trainfit.cz')
            ->setSubject('Dotaz z webové stránky')
            ->setBody('Jméno: ' . $data->name . ' ' . $data->surname . '. Tel. ' . $data->number . '. Text: ' . $data->text);

        $mailer = new SendmailMailer;
        $mailer->send($mail);

        $this->flashMessage('Email byl úspěšně odeslán', 'success');
        $this->redirect('this');
    }
}
