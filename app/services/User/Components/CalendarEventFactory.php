<?php

namespace App\Services\User\Components;

use App\Services\Database\Entities\Admin;
use App\Services\Database\Entities\Event;
use App\Services\Database\Repositories\Events;
use Kdyby\Doctrine\EntityManager;
use Nette\Utils\DateTime;

class CalendarEventFactory
{
    /** @var EntityManager */
    private $em;

    /** @var Events */
    private $events;

    public function __construct(EntityManager $em, Events $events)
    {
        $this->em = $em;
        $this->events = $events;
    }

    public function createAdminEvent($start, $end, $title, $trainer, $trainerBreak, $repeat_on)
    {
        $eventStart = new DateTime($start);
        $eventEnd = new DateTime($end);

        $events = [];

        $event = new Event();
        $event->setUser(null);
        $event->setTrainer($trainer);
        $event->setTitle($title);
        $event->setStart($eventStart);
        $event->setEnd($eventEnd);
        $event->setBreak($trainerBreak);
        $event->setRepeatOn($repeat_on);

        $this->events->create($event);

        $events[] = $event;

        return $events;
    }

    public function createEvent($start, $end, $title, $user, $trainer)
    {
        $eventStart = new DateTime($start);
        $eventEnd = new DateTime($end);

        $events = [];

        $event = new Event();
        $event->setUser($user);
        $event->setTrainer($trainer);
        $event->setTitle($title);
        $event->setStart($eventStart);
        $event->setEnd($eventEnd);
        $event->setBreak(null);

        $this->events->create($event);

        $events[] = $event;

        return $events;
    }

    public function mapEvents($events)
    {
        $calendarEvents = [];

        /** @var Event $event */
        foreach ($events as $event)
        {
            $calendarEvent['id'] = $event->getId();
            $calendarEvent['title'] = $event->getTitle();
            $calendarEvent['start'] = $event->getStart()->format(DateTime::ISO8601);
            $calendarEvent['end'] = $event->getEnd()->format(DateTime::ISO8601);
            if ($event->getUser() === null){
                $calendarEvent['user_id'] = null;
            } else {
                $calendarEvent['user_id'] = $event->getUser()->getId();
            }
            $calendarEvent['trainer_id'] = $event->getTrainer()->getId();
            if ($event->getUser() === null) {
                $calendarEvent['eventUserId'] = null;
            } else {
                $calendarEvent['eventUserId'] = $event->getUser()->getId();
            }
            if ($event->getRepeatOn() !== null) {
                $calendarEvent['repeatOn'] = null;
            } else {
                $calendarEvent['repeatOn'] = $event->getRepeatOn();
            }
            $calendarEvent['trainer_break'] = $event->getBreak();
            $calendarEvents[] = $calendarEvent;
        }

        return $calendarEvents;
    }

    public function getTrainerEvents(Admin $trainer)
    {
        $events = $this->events->getByTrainer($trainer);

        $arrEvents = [];

        /** @var Event $event */
        foreach ($events as $event)
        {
            $calendarEvent['id'] = $event->getId();
            $calendarEvent['title'] = $event->getTitle();
            $calendarEvent['start'] = $event->getStart()->format(DateTime::ISO8601);
            $calendarEvent['end'] = $event->getEnd()->format(DateTime::ISO8601);
            $calendarEvent['user_id'] = null;
            $calendarEvent['trainer_id'] = $event->getTrainer()->getId();
            $calendarEvent['trainer_break'] = $event->getBreak();
            $calendarEvent['repeatOn'] = $event->getRepeatOn();
            $arrEvents[] = $calendarEvent;
        }

        return $arrEvents;
    }

    public function getEventsByTrainer(Admin $trainer)
    {
        $events = $this->events->getByTrainer($trainer);

        $arrEvents = [];

        /** @var Event $event */
        foreach ($events as $event)
        {
            $calendarEvent['id'] = $event->getId();
            $calendarEvent['title'] = $event->getTitle();
            $calendarEvent['start'] = $event->getStart()->format(DateTime::ISO8601);
            $calendarEvent['end'] = $event->getEnd()->format(DateTime::ISO8601);
            if ($event->getUser() === null){
                $calendarEvent['user_id'] = null;
            } else{
                $calendarEvent['user_id'] = $event->getUser()->getId();
            }
            $calendarEvent['trainer_id'] = $event->getTrainer()->getId();
            if ($event->getUser() === null) {
                $calendarEvent['eventUserId'] = null;
            } else {
                $calendarEvent['eventUserId'] = $event->getUser()->getId();
            }
            if ($event->getRepeatOn() !== null) {
                $calendarEvent['repeatOn'] = null;
            } else {
                $calendarEvent['repeatOn'] = $event->getRepeatOn();
            }
            $calendarEvent['trainer_break'] = $event->getBreak();
            $arrEvents[] = $calendarEvent;
        }

        return $arrEvents;
    }
















}