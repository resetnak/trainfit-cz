<?php declare(strict_types = 1);

namespace App\Services\User\Forms;

use Nette\Application\UI\Form;

class ForgottenPasswordFormFactory
{
    public function create()
    {
        $form = new Form();

        $form->addEmail('email', 'Email')
            ->setRequired('Email je povinný');

        $form->addSubmit('submit', 'Resetovat');

        return $form;
    }
}