<?php declare(strict_types = 1);

namespace App\Services\User\Forms;

use Nette\Application\UI\Form;

class TrainerSettingsFormFactory
{
    public function create(): Form
    {
        $form = new Form();

        $form->addEmail('email', 'Email');

        $form->addText('nickname', 'Nickname');

        $form->addPassword('password', 'Heslo');

        $form->addSubmit('save', 'Uložit');

        return $form;
    }
}