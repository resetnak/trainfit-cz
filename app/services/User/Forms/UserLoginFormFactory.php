<?php declare(strict_types = 1);

namespace App\Services\User\Forms;

use Nette\Application\UI\Form;

class UserLoginFormFactory
{
    public function create(): Form
    {
        $form = new Form();

        $form->addText('nickname', 'Přihlašovací jméno')
            ->setRequired('Přihlašovací jméno je povinné.');

        $form->addPassword('password', 'Heslo')
            ->setRequired('Heslo je povinné.');

        $form->addSubmit('enter', 'Vstoupit');

        return $form;
    }
}