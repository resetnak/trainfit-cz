<?php declare(strict_types = 1);

namespace App\Services\User\Forms;

use Nette\Application\UI\Form;

class ChangePasswordFormFactory
{
    public function create()
    {
        $form = new Form();

        $form->addPassword('oldPsswd', 'Staré heslo')
            ->setRequired('Staré heslo je povinné');

        $form->addPassword('newPsswd', 'Nové heslo')
            ->setRequired('Nové heslo je povinné');

        $form->addPassword('newPsswdConfirm', 'Kontrola nového hesla')
            ->setRequired('Kontrola hesla je povinná');

        return $form;
    }
}