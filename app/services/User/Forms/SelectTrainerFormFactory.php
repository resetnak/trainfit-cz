<?php declare(strict_types = 1);

namespace App\Services\User\Forms;

use Nette\Application\UI\Form;

class SelectTrainerFormFactory
{
    public function create(array $trainers): Form
    {
        $form = new Form();

        $form->addSelect('trainer_id', 'Trenér: ', $trainers)
            ->setPrompt('Vyberte trenéra');

        return $form;
    }
}