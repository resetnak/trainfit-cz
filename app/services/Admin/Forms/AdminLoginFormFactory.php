<?php declare(strict_types=1);

namespace App\Services\Admin\Forms;

use Nette\Application\UI\Form;

class AdminLoginFormFactory
{

    public function create(): Form
    {
        $form = new Form();

        $form->addText('login', 'Login')
            ->setRequired();

        $form->addPassword('password', 'Heslo')
            ->setRequired();

        $form->addSubmit('submit', 'Vstoupit');

        return $form;
    }

}
