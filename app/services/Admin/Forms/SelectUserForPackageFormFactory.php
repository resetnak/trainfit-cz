<?php declare(strict_types = 1);

namespace App\Services\Admin\Forms;

use Nette\Application\UI\Form;

class SelectUserForPackageFormFactory
{
    public function create(array $users): Form
    {
        $packages = [
            1 => '1 lekce',
            5 => '1 x 5 lekcí',
            10 => '2 x 5 lekcí',
            15 => '3 x 5 lekcí',
            20 => '4 x 5 lekcí',
        ];

        $form = new Form();

        $form->addSelect('user', 'Uživatel:', $users)
            ->setRequired('Uživatel je povinný');

        $form->addSelect('packages', 'Balíček:', $packages)
            ->setRequired('Musíte vybrat balíček.');

        $form->addSubmit('select', 'Vybrat');

        return $form;
    }
}