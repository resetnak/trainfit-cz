<?php

namespace App\Services\Admin\Forms;

use Nette\Application\UI\Form;

class AdminCalendarModalFormFactory
{
    public function create(): Form
    {
        $repeatOn = [
            0 => 'Denně',
            1 => 'Týdně',
        ];


        $form = new Form();

        $form->addText('title', 'Název');

        $form->addText('start', 'Začátek');

        $form->addText('end', 'Konec');

        $form->addCheckbox('break', 'Přestávka');

        $form->addCheckbox('repeatedly', 'Opakovaně');

        $form->addSelect('repeatOn', 'Kdy: ', $repeatOn);

        $form->addSubmit('create', 'Potvrdit');

        return $form;
    }
}