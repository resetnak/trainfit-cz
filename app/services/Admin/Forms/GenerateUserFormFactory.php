<?php declare(strict_types = 1);

namespace App\Services\Admin\Forms;

use Nette\Application\UI\Form;

class GenerateUserFormFactory
{
    public function create(array $trainers): Form
    {
        $form = new Form();

        $form->addEmail('email', 'Email')
            ->setRequired('Email je povinný.');

        $form->addText('name', 'Jméno')
            ->setRequired('Jméno je povinné');

        $form->addText('surname', 'Příjmení')
            ->setRequired('Příjmení je povinné');

        $form->addCheckboxList('trainers', 'Trenéři', $trainers);

        $form->addSubmit('submit', 'Generovat');

        return $form;
    }
}