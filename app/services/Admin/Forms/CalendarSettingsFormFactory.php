<?php declare(strict_types = 1);

namespace App\Services\Admin\Forms;

use Nette\Application\UI\Form;

class CalendarSettingsFormFactory
{
    public function create()
    {
        $form = new Form();

        $form->addInteger('from', 'Od:')
            ->setRequired();

        $form->addInteger('to', 'Do:')
            ->setRequired();

        return $form;
    }
}