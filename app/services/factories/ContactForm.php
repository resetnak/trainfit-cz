<?php

namespace App\Services\Factories;

use Nette\Application\UI\Form;

class ContactForm
{
	public function create()
	{
		$form = new Form();

		$form->addText('name', 'Jméno')
			->setRequired();

		$form->addText('surname', 'Příjmení')
			->setRequired();

		$form->addEmail('email', 'E-mail')
			->setRequired();

		$form->addText('number', 'Tel. číslo');

		$form->addTextArea('text', 'Zpráva')
			->setRequired();

		$form->addCheckbox('policy', 'Tímto dávám souhlas se zpracováním osobních údajů.')
			->setRequired('Tato položka je povinná.');

		$form->addSubmit('submit', 'Odeslat');

		return $form;
	}
}