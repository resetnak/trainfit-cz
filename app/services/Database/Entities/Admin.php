<?php declare(strict_types = 1);

namespace App\Services\Database\Entities;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Kdyby\Doctrine\Entities\Attributes\Identifier;

/**
 * @ORM\Entity
 * @ORM\Table(name="admin")
 */
class Admin
{
    use Identifier;

    public function __construct()
    {
        $this->createdAt = new DateTime();
        $this->fullName = $this->name . ' ' . $this->surname;
    }

    /**
     * @ORM\OneToMany(targetEntity="Event", mappedBy="trainer")
     */
    protected $events;

    /**
     * @ORM\Column(type="string")
     */
    protected $name;

    /**
     * @ORM\Column(type="string")
     */
    protected $surname;

    /**
     * @ORM\Column(type="string")
     */
    protected $nick;

    /**
     * @ORM\Column(type="string")
     */
    protected $password;

    /**
     * @ORM\Column(type="string")
     */
    protected $email;

    /**
     * @ORM\Column(type="string")
     */
    protected $role;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $hourPrice = 0;

    /**
     * @ORM\Column(type="string")
     */
    protected $fullName;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $createdAt;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $workingFrom;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $workingTo;

    /* ----- ACCESSORS -----*/

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getSurname(): string
    {
        return $this->surname;
    }

    public function setSurname(string $surname): void
    {
        $this->surname = $surname;
    }

    public function getNick(): string
    {
        return $this->nick;
    }

    public function setNick(string $nick): void
    {
        $this->nick = $nick;
    }

    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): void
    {
        $this->password = $password;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function setEmail(string $email): void
    {
        $this->email = $email;
    }

    public function getRole(): string
    {
        return $this->role;
    }

    public function setRole(string $role): void
    {
        $this->role = $role;
    }

    public function getHourPrice(): int
    {
        return $this->hourPrice;
    }

    public function setHourPrice(int $hourPrice): void
    {
        $this->hourPrice = $hourPrice;
    }

    public function getCreatedAt(): DateTime
    {
        return $this->createdAt;
    }

    public function setCreatedAt(DateTime $createdAt): void
    {
        $this->createdAt = $createdAt;
    }

    public function getFullName(): string
    {
        return $this->fullName;
    }

    public function setFullName(string $fullName): void
    {
        $this->fullName = $fullName;
    }

    public function getWorkingFrom()
    {
        return $this->workingFrom;
    }

    public function setWorkingFrom($workingFrom): void
    {
        $this->workingFrom = $workingFrom;
    }

    public function getWorkingTo()
    {
        return $this->workingTo;
    }

    public function setWorkingTo($workingTo): void
    {
        $this->workingTo = $workingTo;
    }
}