<?php declare(strict_types = 1);

namespace App\Services\Database\Entities;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\OneToOne;
use Kdyby\Doctrine\Entities\Attributes\Identifier;

/**
 * @ORM\Entity
 * @ORM\Table(name="event")
 */
class Event
{

    use Identifier;

    /**
     * @ORM\Column(type="string")
     */
    protected $title;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $start;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $end;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="id")
     * @JoinColumn(name="user", referencedColumnName="id")
     */
    protected $user;

    /**
     * @ORM\ManyToOne(targetEntity="Admin", inversedBy="id")
     * @JoinColumn(name="trainer", referencedColumnName="id")
     */
    protected $trainer;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $break;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $repeatOn;

    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title): void
    {
        $this->title = $title;
    }

    public function getStart(): \DateTime
    {
        return $this->start;
    }

    /**
     * @param \DateTime $start
     */
    public function setStart($start): void
    {
        $this->start = $start;
    }

    public function getEnd(): \DateTime
    {
        return $this->end;
    }

    /**
     * @param \DateTime $end
     */
    public function setEnd($end): void
    {
        $this->end = $end;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser($user): void
    {
        $this->user = $user;
    }

    public function getTrainer(): Admin
    {
        return $this->trainer;
    }

    /**
     * @param Admin $trainer
     */
    public function setTrainer($trainer): void
    {
        $this->trainer = $trainer;
    }

    public function getBreak(): ?string
    {
        return $this->break;
    }

    public function setBreak(?string $break): void
    {
        $this->break = $break;
    }

    public function getRepeatOn(): ?string
    {
        return $this->repeatOn;
    }

    public function setRepeatOn($repeatOn): void
    {
        $this->repeatOn = $repeatOn;
    }

    public function entityToArray($events)
    {
        $eventsToCalendar = [];

        /** @var Event $event */
        foreach($events as $event){
            $eventToCalendar['id'] = $event->getId();
            $eventToCalendar['title'] = $event->getTitle();
            $eventToCalendar['start'] = $event->getStart();
            $eventToCalendar['end'] = $event->getEnd();
            $eventToCalendar['trainer'] = $event->getTrainer()->getId();
            $eventToCalendar['user'] = $event->getUser()->getId();
            $eventToCalendar['trainer_break'] = $event->getBreak();
            $eventsToCalendar[] = $eventToCalendar;
        }
        return $eventsToCalendar;
    }

}