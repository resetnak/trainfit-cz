<?php declare(strict_types = 1);

namespace App\Services\Database\Entities;

use Doctrine\ORM\Mapping as ORM;
use Kdyby\Doctrine\Entities\Attributes\Identifier;
use Nette\Utils\DateTime;

/**
 * @ORM\Entity
 * @ORM\Table(name="user")
 */
class User
{

    public function __construct()
    {
        $this->role = 'user';
        $this->generatedAd = new DateTime();
    }

    use Identifier;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $uid;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $first_name;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $last_name;

    /**
     * @ORM\Column(type="string")
     */
    protected $nickname;

    /**
     * @ORM\Column(type="string")
     */
    protected $fullName;

    /**
     * @ORM\Column(type="string")
     */
    protected $password;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $email;

    /**
     * @ORM\Column(type="string")
     */
    protected $role;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $createdAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $generatedAd;

    /**
     * @ORM\OneToMany(targetEntity="Event", mappedBy="user")
     */
    protected $events;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $packageAmount = 0;

    /**
     * @ORM\Column(type="array", nullable=true)
     */
    protected $allowedTrainers;

    // ---- Accessors ----

    public function getUid(): ?string
    {
        return $this->uid;
    }

    public function setUid($uid): void
    {
        $this->uid = $uid;
    }

    public function getFirstName(): ?string
    {
        return $this->first_name;
    }

    public function setFirstName($first_name): void
    {
        $this->first_name = $first_name;
    }

    public function getLastName(): ?string
    {
        return $this->last_name;
    }

    public function setLastName($last_name): void
    {
        $this->last_name = $last_name;
    }

    public function getNickname(): ?string
    {
        return $this->nickname;
    }

    public function setNickname($nickname): void
    {
        $this->nickname = $nickname;
    }

    public function getFullName(): string
    {
        return $this->fullName;
    }

    public function setFullName(string $fullName): void
    {
        $this->fullName = $fullName;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword($password): void
    {
        $this->password = $password;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail($email): void
    {
        $this->email = $email;
    }

    public function getRole(): ?string
    {
        return $this->role;
    }

    public function setRole($role): void
    {
        $this->role = $role;
    }

    public function getCreatedAt(): ?\DateTime
    {
        return $this->createdAt;
    }

    public function setCreatedAt(DateTime $createdAt): void
    {
        $this->createdAt = $createdAt;
    }

    public function getGeneratedAd(): ?\DateTime
    {
        return $this->generatedAd;
    }

    public function setGeneratedAd(DateTime $generatedAd): void
    {
        $this->generatedAd = $generatedAd;
    }

    public function getPackageAmount(): ?int
    {
        return $this->packageAmount;
    }

    public function setPackageAmount(int $packageAmount): void
    {
        $this->packageAmount = $packageAmount;
    }

    public function addPackageAmount(int $amount): void
    {
        $this->packageAmount += $amount;
    }

    public function removeOnePackage()
    {
        $this->packageAmount = $this->packageAmount - 1;
    }

    public function addOnePackage()
    {
        $this->packageAmount = $this->packageAmount + 1;
    }

    public function getAllowedTrainers()
    {
        return $this->allowedTrainers;
    }

    public function setAllowedTrainers($allowedTrainers): void
    {
        $this->allowedTrainers = $allowedTrainers;
    }

}