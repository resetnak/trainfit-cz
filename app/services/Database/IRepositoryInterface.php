<?php declare(strict_types = 1);

namespace App\Services\Database;

interface IRepositoryInterface
{
    public function create($entity): void;

    public function delete($entity): void;
}