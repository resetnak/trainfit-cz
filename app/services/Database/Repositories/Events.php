<?php declare(strict_types = 1);

namespace App\Services\Database\Repositories;

use App\Services\Database\Entities\Admin;
use App\Services\Database\Entities\Event;
use Kdyby\Doctrine\EntityManager;
use Kdyby\Doctrine\EntityRepository;
use Nette\Utils\DateTime;

class Events
{
    /** @var EntityManager $em */
    private $em;

    /** @var EntityRepository $events */
    private $events;

    /** @param EntityManager $em */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
        $this->events = $this->em->getRepository(Event::class);
    }

    public function create($entity): void
    {
        $this->em->persist($entity);
        $this->em->flush();
    }

    public function delete($entity): void
    {
        $this->em->remove($entity);
        $this->em->flush();
    }

    public function update($entity): void
    {
        $this->em->persist($entity);
        $this->em->flush();
    }

    public function getByTrainer(Admin $trainer)
    {
        return $this->events->findBy(['trainer' => $trainer]);
    }

    public function getById(int $id)
    {
        return $this->events->find($id);
    }

    public function getBookEventsUntilNowByTrainer(Admin $admin)
    {
        // First day of this month
        $d = new DateTime('first day of this month');
        $d = $d->format('Y-m-d');

        $date = date('Y-m-d');

        return $qb = $this->events->createQueryBuilder('events')
            ->select('events.id')
            ->where('events.break = :break')
            ->andWhere('events.trainer = :trainer')
            ->andWhere('events.start > :start AND events.end < :end')
            ->setParameter('break', 'no')
            ->setParameter('trainer', $admin)
            ->setParameter('start', $d)
            ->setParameter('end', $date)
            ->getQuery()
            ->getResult();
    }

}