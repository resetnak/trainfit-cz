<?php declare(strict_types = 1);

namespace App\Services\Database\Repositories;


use App\Services\Database\Entities\Admin;
use Kdyby\Doctrine\EntityManager;
use Kdyby\Doctrine\EntityRepository;

class Admins
{
    /** @var EntityManager $em */
    private $em;

    /** @var EntityRepository $admins */
    private $admins;

    /** @param EntityManager $em */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
        $this->admins = $this->em->getRepository(Admin::class);
    }

    public function create($entity): void
    {
        $this->em->persist($entity);
        $this->em->flush();
    }

    public function update($entity): void
    {
        $this->em->persist($entity);
        $this->em->flush();
    }

    public function delete($entity): void
    {
        $this->em->remove($entity);
        $this->em->flush();
    }

    public function getByNick(string $nick): ?Admin
    {
        return $this->admins->findOneBy([
            'nick' => $nick
        ]);
    }

    public function getById(int $id)
    {
        return $this->admins->find($id);
    }

    public function getTrainersForSelect(): array
    {
        return $this->admins->findPairs([], 'fullName', 'id');
    }

    public function getForFrontPriceList(): array
    {
        return $this->admins->createQueryBuilder('adm')
            ->select('adm.fullName, adm.hourPrice')
            ->where('adm.id != :id1')
            ->andWhere('adm.id != :id2')
            ->andWhere('adm.id != :id3')
            ->setParameter('id1', 1)
            ->setParameter('id2', 2)
            ->setParameter('id3', 7)
            ->getQuery()
            ->getResult();
    }

    public function getAll()
    {
        return $this->admins->findAll();
    }

    public function getForGrid()
    {
        return $this->admins->createQueryBuilder('admin')
            ->select('admin')
            ->where('admin.id != :id1 AND admin.id != :id2')
            ->setParameter('id1', 1)
            ->setParameter('id2', 2)
            ->getQuery()
            ->getResult();
    }

    public function getRepeatedEventsByTrainerId($trainerId)
    {
        $qb = $this->admins->createQueryBuilder('qb');

        return $qb->select('qb')
            ->where('qb.repeatEvents IS NOT NULL')
            ->andWhere('qb.id = :id')
            ->setParameter('id', $trainerId)
            ->getQuery()
            ->getResult();
    }

}