<?php declare(strict_types = 1);

namespace App\Services\Database\Repositories;

use App\Services\Database\Entities\User;
use Kdyby\Doctrine\EntityManager;
use Kdyby\Doctrine\EntityRepository;

class Users
{
    /** @var EntityManager $em */
    private $em;

    /** @var EntityRepository $users */
    private $users;

    /** @param EntityManager $em */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
        $this->users = $this->em->getRepository(User::class);
    }

    public function create($entity): void
    {
        $this->em->persist($entity);
        $this->em->flush();
    }

    public function update($entity): void
    {
        $this->em->persist($entity);
        $this->em->flush();
    }

    public function delete($entity): void
    {
        $this->em->remove($entity);
        $this->em->flush();
    }

    public function getByNick(string $nick): ?User
    {
        return $this->users->findOneBy([
            'nickname' => $nick
        ]);
    }

    public function getByEmail(string $email): ?User
    {
        return $this->users->findOneBy([
            'email' => $email
        ]);
    }

    public function getById(int $id)
    {
        return $this->users->find($id);
    }

    public function isUidUnique(string $uid): bool
    {
        $result = $this->users->findOneBy([
            'uid' => $uid
        ]);

        if ($result === null) return true;
        else return false;
    }

    public function getUsersForSelect(): array
    {
        return $this->users->findPairs([], 'fullName', 'id');
    }

    public function getAll()
    {
        return $this->users->findAll();
    }

}