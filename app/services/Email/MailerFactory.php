<?php declare(strict_types = 1);

namespace App\Services\Email;

use Nette\Mail\Message;
use Nette\Mail\SmtpMailer;

class MailerFactory
{
    const CONFIG = [
        'host' => 'smtp-224127.m27.wedos.net',
        'username' => 'mail@trainfit.cz',
        'password' => 'Mal123%%',
        'port' => 587,
        'secure' => 'tls'
    ];

    public function send(Message $message)
    {
        $mailer = new SmtpMailer(self::CONFIG);
        $mailer->send($message);
    }
}