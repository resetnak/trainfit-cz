<?php declare(strict_types = 1);

namespace App\UserModule\Presenters;

use App\Services\Database\Repositories\Users;
use Nette\Security\AuthenticationException;
use Nette\Security\Identity;
use Nette\Security\Passwords;
use Nette\Security\User;

class UserAuthenticator
{

    /** @var Users */
    private $_users;

    /** @var User */
    private $user;

    public function __construct(User $user, Users $_users)
    {
        $this->user = $user;
        $this->_users = $_users;
    }

    public function login($username, $password)
    {
        /** @var User $userEntity */
        $userEntity = $this->_users->getByNick($username);

        // User exists?
        if (!$userEntity instanceof \App\Services\Database\Entities\User)
        {
            throw new AuthenticationException('Špatně zadané údaje.');
        }

        // First time access?
        if ($userEntity->getCreatedAt() === null)
        {
            // Verify password sent by email
            if ($password === $userEntity->getPassword())
            {
                $this->user->login(new Identity($userEntity->getId(), null, [
                    'id' => $userEntity->getId(),
                    'first_name' => $userEntity->getFirstName(),
                    'last_name' => $userEntity->getLastName(),
                    'email' => $userEntity->getEmail(),
                    'generated' => $userEntity->getGeneratedAd(),
                    'created' => $userEntity->getCreatedAt(),
                ]));
            }
        }
        else
        {
            $verify = Passwords::verify($password, $userEntity->getPassword());

            if (!$verify)
            {
                throw new AuthenticationException('Špatně zadané údaje.');
            }

            $this->user->login(new Identity($userEntity->getId(), null, [
                'id' => $userEntity->getId(),
                'first_name' => $userEntity->getFirstName(),
                'last_name' => $userEntity->getLastName(),
                'email' => $userEntity->getEmail(),
                'generated' => $userEntity->getGeneratedAd(),
                'created' => $userEntity->getCreatedAt(),
            ]));
        }
    }

}