<?php declare(strict_types = 1);

namespace App\UserModule\Presenters;

use App\Services\Database\Entities\Admin;
use App\Services\Database\Entities\Event;
use App\Services\Database\Entities\User;
use App\Services\User\Components\CalendarEventFactory;
use App\Services\User\Forms\SelectTrainerFormFactory;
use Nette\Application\UI\Form;
use function GuzzleHttp\Promise\all;

class CalendarPresenter extends BasePresenter
{
    /** @var SelectTrainerFormFactory @inject */
    public $selectTrainerFormFactory;

    /** @var CalendarEventFactory @inject */
    public $calendarEventFactory;

    public function handleSaveEvent($start, $end, $title, $trainerId)
    {
        if($this->isAjax())
        {
            /** @var User $user */
            $user = $this->users->getById($this->user->getIdentity()->getId());
            $trainer = $this->admins->getById((int)$trainerId);
            $user->removeOnePackage();
            $this->users->update($user);

            $newEvent = $this->calendarEventFactory->createEvent($start, $end, $title, $user, $trainer);
            $this->template->calendarEvents = $this->calendarEventFactory->mapEvents($newEvent);

            $this->redrawControl('calendar');
            $this->redirect('this');
        }

    }

    public function handleDeleteEvent($eventId)
    {
        /** @var Event $event */
        $event = $this->events->getById((int)$eventId);

        $user = $event->getUser();
        $user->addOnePackage();

        $this->users->update($user);

        $this->events->delete($event);

        $this->redirect('this');
    }

    public function handleUnselectEvent()
    {
        $this->redirect('this');
    }

    public function renderShow($trainerId)
    {
        $trainerId = (int) $this->getParameter('trainerId');

        /** @var User $user */
        $user = $this->users->getById($this->getUser()->getIdentity()->getId());
        $allowedTrainers = $user->getAllowedTrainers();

        $isAllowed = $this->isInAllowedTrainers($allowedTrainers, $trainerId);

        if (!$isAllowed) {
            $this->redirect(':User:Calendar:');
        } else {

            /** @var Admin $trainer */
            $trainer = $this->admins->getById((int)$trainerId);

            $this->template->trainerId = (int) $trainerId;

            $this->template->userId = $user->getId();

            $this->template->calendarEvents = $this->calendarEventFactory->getEventsByTrainer($trainer);
            $this->template->trainerName = $trainer->getFullName();
            $this->template->userName = $user->getFirstName() . ' ' . $user->getLastName();
            $this->template->packages = $user->getPackageAmount();
            $this->template->schedule = [$trainer->getWorkingFrom(), $trainer->getWorkingTo()];

        }
    }

    public function createComponentSelectTrainerForm(): Form
    {
        /** @var User $actualUser */
        $actualUser = $this->users->getById($this->getUser()->getIdentity()->getId());

        $allowedTrainers = $actualUser->getAllowedTrainers();
        $trainers = [];

        foreach ($allowedTrainers as $key => $value){
            $trainers[$value] = $this->admins->getById($value)->getFullName();
        }

        $form = $this->selectTrainerFormFactory->create($trainers);
        $form->onSuccess[] = [$this, 'processSelectTrainer'];

        return $form;
    }

    public function processSelectTrainer(Form $form)
    {
        $data = $form->getValues();

        $trainerId = $data->trainer_id;

        if($trainerId !== null) {
            $this->redirect(':show', $trainerId);
        }
    }

    private function isInAllowedTrainers(array $allowedTrainers, $trainerId): bool
    {
        if (in_array($trainerId, $allowedTrainers)) return true;

        return false;
    }

}