<?php

namespace App\UserModule\Presenters;

use App\Services\Database\Entities\Event;
use App\Services\Database\Repositories\Admins;
use App\Services\Database\Repositories\Events;
use App\Services\Database\Repositories\Users;
use App\Services\Email\MailerFactory;
use Kdyby\Translation\Translator;
use Nette\Application\UI\Presenter;

abstract class BasePresenter extends Presenter
{
	/** @var @persistent */
	public $locale;

	/** @var Translator @inject */
	public $translator;

	/** @var Admins @inject */
	public $admins;

    /** @var MailerFactory @inject */
    public $mailer;

	/** @var Users @inject */
	public $users;

	/** @var Events @inject */
	public $events;

	/** @var Event @inject */
	public $event;

    protected function startup()
    {
        parent::startup();

        $this->setLayout(__DIR__ . '/../../template/@user.latte');

        if (!$this->user->isLoggedIn())
        {
            if (!$this->presenter->isLinkCurrent(':User:User:'))
            {
                $this->flashMessage('Přihlaste se prosím', 'warning');
                $this->redirect(':User:User:');
            }
        }

        if ($this->user->isLoggedIn() && $this->presenter->isLinkCurrent(':User:User:')){
            $this->redirect(':User:User:dashboard');
        }
    }

    // Handle logout
    public function handleLogout(): void
    {
        $this->user->logout(true);
        $this->redirect(':Public:Homepage:');
    }
}