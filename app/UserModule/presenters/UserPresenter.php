<?php declare(strict_types=1);

namespace App\UserModule\Presenters;

use App\Services\Database\Entities\User;
use App\Services\Database\Repositories\Users;
use App\Services\User\Forms\ChangePasswordFormFactory;
use App\Services\User\Forms\ForgottenPasswordFormFactory;
use App\Services\User\Forms\SetNewUserFormFactory;
use App\Services\User\Forms\UserLoginFormFactory;
use Nette\Application\UI\Form;
use Nette\Mail\Message;
use Nette\Mail\SendmailMailer;
use Nette\Security\AuthenticationException;
use Nette\Security\Passwords;
use Nette\Utils\DateTime;
use Nette\Utils\Random;
use Tracy\Debugger;

class UserPresenter extends BasePresenter
{
    /** @var UserAuthenticator @inject */
    public $userAuthenticator;

    /** @var UserLoginFormFactory @inject */
    public $userLoginFormFactory;

    /** @var SetNewUserFormFactory @inject */
    public $setNewUserFormFactory;

    /** @var ChangePasswordFormFactory @inject */
    public $changePasswordFormFactory;

    /** @var ForgottenPasswordFormFactory @inject */
    public $forgottenPasswordFormFactory;

    /** @var Users @inject */
    public $users;

    /** @var User */
    public $loggedUser;

    public function startup()
    {
        // Parent call
        parent::startup();
        // Force user to log in before any action
        if (!$this->user->isLoggedIn()) {
            if (!$this->presenter->isLinkCurrent(':default')) {
                $this->flashMessage('Přihlašte se prosím', 'info');
                $this->redirect(':default');
            }
        }
        // Force user to set the credentials
        if ($this->user->isLoggedIn() && !$this->presenter->isLinkCurrent(':newUser')) {
            $user = $this->users->getById($this->user->getIdentity()->getId());
            if ($user->getCreatedAt() === null) {
                $this->redirect(':newUser');
            }
        }
    }

    public function createComponentForgottenPasswordForm()
    {
        return $this->forgottenPasswordFormFactory->create();
    }

    public function handleForgottenPassword(string $email)
    {
        if ($this->isAjax()){

            /** @var User $user */
            $user = $this->users->getByEmail($email);

            if ($user instanceof User) {

                $password = Random::generate(8);
                $user->setPassword(Passwords::hash($password));
                $this->users->update($user);

                $mail = new Message;
                $mail->setFrom('mail@trainfit.cz')
                    ->addTo($email)
                    ->setSubject('Resetování hesla | TrainFit')
                    ->setBody("Přišel požadavek na resetování Vašeho hesla.\nVaše nové heslo je: " . $password . "\nS přáním\nTrainFit.cz");

                $this->mailer->send($mail);

                $this->payload->succ = true;
            } else {
                $this->payload->err = true;
            }
            $this->sendPayload();
        }
    }

    public function renderConfig(): void
    {
        $this->template->user = $this->users->getById($this->user->getIdentity()->getId());
    }

    public function createComponentUserLogin(): Form
    {
        $form = $this->userLoginFormFactory->create();
        $form->onSuccess[] = [$this, 'processUserLogin'];

        return $form;
    }

    public function processUserLogin(Form $form)
    {
        $data = $form->getValues();
        try {
            $this->userAuthenticator->login($data->nickname, $data->password);
            $this->user->setExpiration('100 minutes');
            if ($this->user->isLoggedIn()) {

                $user = $this->users->getByNick($data->nickname);

                if ($user->getCreatedAt() === null) {
                    $this->redirect(':newUser');
                }
                $this->redirect(':default');
            } else {
                $this->flashMessage('Špatně zadané údaje', 'warning');
                $this->redirect('this');
            }
        } catch (AuthenticationException $exception) {
            $this->flashMessage('Špatně zadané jméno nebo heslo.', 'warning');
            $this->redirect('this');
        }
    }

    public function createComponentSetNewUser(): Form
    {
        $form = $this->setNewUserFormFactory->create();
        $form->onSuccess[] = [$this, 'processSetNewUser'];

        return $form;
    }

    public function processSetNewUser(Form $form)
    {
        $data = $form->getValues();

        /** @var User $user */
        $user = $this->users->getById($this->user->getIdentity()->getId());
        $user->setNickname($data->nickname);
        $user->setPassword(Passwords::hash($data->password));
        $user->setCreatedAt(new DateTime());

        do {
            $uid = Random::generate(12);
        } while ($this->users->isUidUnique($uid) === false);

        $user->setUid($uid);
        $this->users->update($user);
        $this->flashMessage('Vaše údaje byly nastaveny. Děkujeme. Bylo Vám vygenerováno unikátní ID: ' . $uid . ' Naleznete ho v uživatelských informacích.', 'info');
        $this->redirect(':overview');
    }

    public function createComponentChangePasswordForm()
    {
        $form = $this->changePasswordFormFactory->create();
        $form->onSubmit[] = [$this, 'processChangePassword'];

        return $form;
    }

    public function processChangePassword(Form $form)
    {
        $data = $form->getValues();

        /** @var User $actualUser */
        $actualUser = $this->users->getById($this->getUser()->getIdentity()->getId());

        if (Passwords::verify($data->oldPsswd, $actualUser->getPassword())) {

            if ($data->newPsswd === $data->newPsswdConfirm) {

                $actualUser->setPassword(Passwords::hash($data->newPsswd));
                $this->users->update($actualUser);
                $this->flashMessage('Heslo úspěšně změněno', 'success');
                $this->redirect('this');

            } else {
                $this->flashMessage('Nové heslo není stejné');
            }
        } else {
            $this->flashMessage('Zadal jste špatné heslo');
        }

    }


}